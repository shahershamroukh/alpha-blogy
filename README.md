
![](https://img.shields.io/badge/Alphablogy-blueviolet)

# Alpha-Blogy

> The application allows users to log in and write articles and edit them as well as commenting on other people's articles and more.

![screenshot](./app/assets/images/alpha.png)

## Built With

- Ruby
- Ruby on rails
- bootstrap
- postgresql

## Live Demo

[Live Demo Link](https://alphablogy.herokuapp.com/)



To get a local copy up and running, follow these simple example steps.

### ruby 3

### ruby on rails 6

### Install yarn

### Run rails s


## Authors

👤 **Shaher**

- GitHub: [@githubhandle](https://github.com/Shaher-11)
- Twitter: [@twitterhandle](https://twitter.com/ShaherShamroukh/)
- LinkedIn: [LinkedIn](https://www.linkedin.com/in/shaher-shamroukh/)

## Show your support

Give a ⭐️ if you like this project!

## Acknowledgments

- Rails guide

## 📝 License

This project is [MIT](lic.url) licensed.
